package settings

import (
	"context"
	"fmt"
	"github.com/GuiaBolso/darwin"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

type Settings struct {
	db            *sqlx.DB
	redisCache    *redis.Client
	pGuildsJoined *prometheus.GaugeVec
}

func Setup() (*Settings, error) {
	db, err := sqlx.Connect("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		return nil, err
	}

	logrus.Info("Connected to the database")

	// do migrations
	driver := darwin.NewGenericDriver(db.DB, darwin.PostgresDialect{})
	migrator := darwin.New(driver, migrations, nil)

	err = migrator.Migrate()
	if err != nil {
		return nil, err
	}

	logrus.Info("Migrations complete!")

	// now connect to redis
	rCache := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDR"),
		Username: os.Getenv("REDIS_USER"),
		Password: os.Getenv("REDIS_PASS"),
		DB:       0,
	})

	// ping to be sure
	err = rCache.Ping(context.Background()).Err()
	if err != nil {
		return nil, err
	}

	// we do this here and then use it in events
	pGuildsJoined := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "guildcrius",
		Subsystem: "settings",
		Name:      "guilds_joined",
		Help:      "Number of guilds joined, partitioned by platform",
	}, []string{"platform"})

	settings := &Settings{
		db:            db,
		redisCache:    rCache,
		pGuildsJoined: pGuildsJoined,
	}

	return settings, nil
}

func (s *Settings) GetDB() *sqlx.DB {
	return s.db
}

func (s *Settings) GetRedisCache() *redis.Client {
	return s.redisCache
}

// Returns the val in the cache (as a *string, you may need to convert), true if a cache hit, an error
func (s *Settings) CheckRedisCache(rootKey string, subKey interface{}) (*string, bool, error) {
	key := fmt.Sprintf("%s:%s", rootKey, subKey)

	res, err := s.redisCache.Get(context.Background(), key).Result()
	if err == redis.Nil {
		return nil, false, nil
	}
	if err != nil {
		return nil, false, err
	}

	return &res, true, nil
}

func (s *Settings) AddToCache(rootKey string, subKey interface{}, val interface{}, ttl time.Duration) error {
	key := fmt.Sprintf("%s:%s", rootKey, subKey)
	return s.redisCache.Set(context.Background(), key, val, ttl).Err()
}
