package settings

import (
	"context"
	"github.com/asaskevich/EventBus"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/RPGPN/criuscommander/v2/discord"
)

// going around the side way to register some events because settings cannot.
func (s *Settings) EventsSetup(ctx context.Context) (map[string]criuscommander.CommandHandler, error) {
	bus := ctx.Value("bus").(EventBus.BusSubscriber)

	bus.Subscribe(discord.EventGuildCreate, s.onGuildJoin)
	bus.Subscribe(discord.EventGuildDelete, s.onGuildLeave)

	return nil, nil
}

func (s *Settings) onGuildJoin(g *discordgo.GuildCreate) {
	s.pGuildsJoined.WithLabelValues(criuscommander.PlatformDiscord.ToString()).Inc()

	var sid *string
	s.db.Get(&sid, "SELECT server_id FROM servers WHERE server_id=$1", g.ID)

	if sid == nil {
		// we need to add a new row to the database
		_, err := s.db.Exec("INSERT INTO servers (server_id, owner_id) VALUES ($1, $2) ON CONFLICT do nothing ",
			g.ID, g.OwnerID)

		if err != nil {
			logrus.Error(err)
		}
	}
}

func (s *Settings) onGuildLeave(_ *discordgo.GuildDelete) {
	s.pGuildsJoined.WithLabelValues(criuscommander.PlatformDiscord.ToString()).Dec()
}
