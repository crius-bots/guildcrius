package settings

import "github.com/GuiaBolso/darwin"

var migrations = []darwin.Migration{
	{
		Version:     1,
		Description: "Create table servers",
		Script: `CREATE TABLE servers (
			server_id          text not null primary key,
			owner_id 		   text not null
		);`,
	},
	{
		Version:     2,
		Description: "Create table server_permissions",
		Script: `CREATE TABLE server_permissions (
			perm_id    serial primary key,
			server_id  text not null references servers,
			permission integer  not null,
			user_id    text  not null
		);`,
	},
	{
		Version:     3,
		Description: "Create firehose platforms enum",
		Script:      "CREATE TYPE firehose_platforms AS enum ('glimesh', 'twitch', 'youtube');",
	},
	{
		Version:     3.1,
		Description: "Create table firehose__notifs",
		Script: `CREATE TABLE firehose__notifs (
			notif_id serial primary key,
			realm_id text not null,
			channel_id text not null,
			ext_id text not null,
			ext_platform firehose_platforms not null,
			message text not null
		);`,
	},
	{
		Version:     3.2,
		Description: "Add column platform to firehose__notifs",
		Script:      "ALTER TABLE firehose__notifs ADD COLUMN platform text not null default 'discord';",
	},
	{
		Version:     3.3,
		Description: "Rename ext_id to service",
		Script:      "ALTER TABLE firehose__notifs RENAME ext_platform TO service;",
	},
	{
		Version:     4,
		Description: "create table pres_cycle",
		Script: `CREATE TABLE pres_cycle (
			id serial not null,
			pres text not null,
			status_type text not null,
			was_last_used bool default false not null,
			constraint pres_cycle_pk
				primary key (status_type, pres)
		);`,
	},
	{
		Version:     5,
		Description: "create table reaction_roles",
		Script: `CREATE TABLE reaction_roles (
			rr_id      serial primary key,
			server_id  text not null,
			message_id text not null,
			give_role  text not null,
			emoji      text not null
		);`,
	},
	{
		Version:     6,
		Description: "Create table mailer__mailbox",
		Script: `CREATE TABLE mailer__mailbox (
			mailbox_id   serial primary key,
			server_id    text not null,
			channel_id   text not null,
			mailbox_name text not null
		);`,
	},
	{
		Version:     7,
		Description: "Create table firehose_i_subs",
		Script: `CREATE TABLE firehose_i_subs (
			sub_id text not null primary key,
			service firehose_platforms not null,
			to_cond text not null,
			to_event text not null
		);`,
	},
}
