package main

import (
	"context"
	"github.com/getsentry/sentry-go"
	"github.com/makasim/sentryhook"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/RPGPN/crius/guildcrius/permissions"
	"gitlab.com/RPGPN/crius/guildcrius/settings"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/RPGPN/criuscommander/v2/discord"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func init() {
	level := os.Getenv("LOG_LVL")

	if level == "" {
		level = "info"
	}

	intLevel, err := logrus.ParseLevel(level)
	if err != nil {
		panic(err)
	}

	logrus.SetLevel(intLevel)
}

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:              os.Getenv("SENTRY_DSN"),
		AttachStacktrace: true,
		Environment:      os.Getenv("SENTRY_ENV"),
	})
	if err != nil {
		panic(err)
	}
	defer sentry.Flush(2 * time.Second)

	sentryHook := sentryhook.New([]logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel})

	logrus.AddHook(sentryHook)

	gcSettings, err := settings.Setup()
	if err != nil {
		panic(err)
	}

	gcPermissions := permissions.Setup()

	// build up context
	ctx := context.WithValue(context.Background(), "settings", gcSettings)
	ctx = context.WithValue(ctx, "permissions", gcPermissions)

	srv := &http.Server{Addr: ":" + os.Getenv("PORT")}
	http.Handle("/metrics", promhttp.Handler())

	cmder, err := cc.NewCommander(&cc.CCConfig{
		Prefix: os.Getenv("PREFIX"),
		Tokens: map[cc.PlatformType]string{
			cc.PlatformDiscord: os.Getenv("DISCORD_TOKEN"),
		},
		PluginLoaders:   cc.DefaultLoaderChain,
		AutoloadPlugins: false,
		Platforms:       []cc.Platform{discord.New()},
		OtherConfig: map[string]interface{}{
			"DiscordOwner": os.Getenv("DSC_OWNER_ID"),
			"URL":          "https://gitlab.com/RPGPN/crius/guildcrius",
		},
	}, ctx)
	if err != nil {
		panic(err)
	}

	err = cmder.Open()
	if err != nil {
		panic(err)
	}

	cmder.SetupPluginFromConfig(gcSettings.EventsSetup, &cc.PluginJSON{Name: "_events", SupportedPlatforms: cc.PlatformDiscord, HideInHelp: true})
	cmder.SetupPluginFromConfig(gcPermissions.Setup, permissions.Info)

	go srv.ListenAndServe()

	err = loadPlugins(cmder)
	if err != nil {
		panic(err)
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	cmder.Close()
	srv.Close()
}
