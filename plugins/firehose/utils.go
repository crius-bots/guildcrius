package firehose

import (
	"bytes"
	"errors"
	"github.com/jmoiron/sqlx"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius/guildcrius/settings"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"regexp"
	"text/template"
)

var (
	youtubeChannelURLRegex = regexp.MustCompile("(?:(?:https?://)?(?:www.)?youtube.com/channel/)([A-z0-9-_]{24})/?")
	ErrNoYTMatch           = errors.New("a valid channel url is required for youtube in the format https://youtube.com/channel/[channel_id]")
)

func getExtID(service firehoseService, name string, s *settings.Settings) (*string, error) {
	var (
		id  *string
		err error
	)
	switch service {
	case _FirehoseServiceGlimesh:
		id, err = glimGetChannelIDFromUsername(name, s)
	case _FirehoseServiceTwitch:
		id, err = twitchGetChannelIDFromUsername(name, s)
	case _FirehoseServiceYoutube:
		matches := youtubeChannelURLRegex.FindStringSubmatch(name)
		if len(matches) < 2 {
			// the regex hasn't found a channel ID, let's send them the usage
			return nil, ErrNoYTMatch
		}
		id = &matches[1]
	}

	if err != nil {
		return nil, err
	}

	return id, nil
}

func makeAndCompileTemplate(content string, data *fhGoLive) ([]byte, error) {
	msgTempl, err := template.New("").Parse(content)
	if err != nil {
		return nil, err
	}

	// make a buffer
	var buf bytes.Buffer

	// and exec the template with the data
	err = msgTempl.Execute(&buf, data)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type notifyServer struct {
	ChannelID string `db:"channel_id"`
	Message   string
}

func getServersToNotify(db *sqlx.DB, extID string, service firehoseService) ([]*notifyServer, error) {
	toNotify := []*notifyServer{}

	err := db.Select(&toNotify, "SELECT channel_id, message FROM firehose__notifs WHERE ext_id=$1 AND service=$2",
		extID, service)
	if err != nil {
		return nil, err
	}

	return toNotify, nil
}

func (f *firehose) doNotifyServers(toNotify []*notifyServer, data *fhGoLive) error {
	commander := CriusUtils.GetCommander(f.ctx)

	for _, notify := range toNotify {
		compiled, err := makeAndCompileTemplate(notify.Message, data)
		if err != nil {
			return err
		}

		f.pNotificationsSent.WithLabelValues(cc.PlatformDiscord.ToString(), string(_FirehoseServiceTwitch)).Inc()

		_, err = commander.SendMessage(cc.PlatformDiscord, notify.ChannelID, string(compiled))
		if err != nil {
			return err
		}
	}

	return nil
}
