package firehose

import "strings"

type firehoseService string

const (
	_FirehoseServiceGlimesh firehoseService = "glimesh"
	_FirehoseServiceTwitch  firehoseService = "twitch"
	_FirehoseServiceYoutube firehoseService = "youtube"
)

func (f firehoseService) IsValid() bool {
	lower := strings.ToLower(string(f))

	switch firehoseService(lower) {
	case _FirehoseServiceGlimesh, _FirehoseServiceTwitch, _FirehoseServiceYoutube:
		return true
	default:
		return false
	}
}
