package firehose

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/xml"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

const (
	_YoutubeFeed        = "https://www.youtube.com/xml/feeds/videos.xml?channel_id=%s"
	_AppSpotHub         = "https://pubsubhubbub.appspot.com/subscribe"
	_YoutubeLeaseLength = 1 * 60 * 60 * 24 // in seconds (one day)
)

type YTFeed struct {
	XMLName xml.Name `xml:"http://www.w3.org/2005/Atom feed"`
	Title   string   `xml:"title"`
	Updated string   `xml:"updated"`
	Entry   struct {
		ID        string `xml:"id"`
		VideoId   string `xml:"videoId"`
		ChannelId string `xml:"channelId"`
		Title     string `xml:"title"`
		Link      struct {
			Href string `xml:"href,attr"`
		} `xml:"link"`
		Author struct {
			Name string `xml:"name"`
			URI  string `xml:"uri"`
		} `xml:"author"`
		Published string `xml:"published"`
		Updated   string `xml:"updated"`
	} `xml:"entry"`
}

func (f *firehose) ytBeginSub(id string) error {
	body := url.Values{}

	body.Add("hub.callback", os.Getenv("WEBSUB_URL")+"/firehose/sub/youtube")
	body.Add("hub.mode", "subscribe")
	body.Add("hub.topic", fmt.Sprintf(_YoutubeFeed, id))
	body.Add("hub.secret", os.Getenv("YT_HOOK_SECRET"))
	body.Add("hub.lease_seconds", strconv.Itoa(_YoutubeLeaseLength))

	req, err := http.NewRequest("POST", _AppSpotHub, bytes.NewReader([]byte(body.Encode())))
	if err != nil {
		return err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusAccepted {
		resBody, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}

		logrus.Error(string(resBody))
		return nil
	}

	return nil
}

func (f *firehose) ytEndSub(id string) error {
	body := url.Values{}

	body.Add("hub.callback", os.Getenv("WEBSUB_URL")+"/firehose/sub/youtube")
	body.Add("hub.mode", "unsubscribe")
	body.Add("hub.topic", fmt.Sprintf(_YoutubeFeed, id))

	req, err := http.NewRequest("POST", _AppSpotHub, bytes.NewReader([]byte(body.Encode())))
	if err != nil {
		return err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusAccepted {
		resBody, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}

		logrus.Error(string(resBody))
		return nil
	}

	return nil
}

func youtubeCheckSignature(r *http.Request, body []byte) (bool, error) {
	// what signature do we expect? (it's hex encoded)
	expectedHexString := r.Header.Get("X-Hub-Signature")
	if len(expectedHexString) < 5 {
		return false, nil
	}

	expectedBytes, err := hex.DecodeString(expectedHexString[5:])
	if err != nil {
		return false, err
	}

	hasher := hmac.New(sha1.New, []byte(os.Getenv("YT_HOOK_SECRET")))
	_, err = hasher.Write(body)
	if err != nil {
		return false, err
	}

	signature := hasher.Sum(nil)

	if !hmac.Equal(signature, expectedBytes) {
		return false, nil
	}

	return true, nil
}

func (f *firehose) youtubeEndpoint(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		// it's a challenge
		challenge := r.URL.Query().Get("hub.challenge")
		w.Write([]byte(challenge))
		return
	}

	// read the body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Error(err)
		return
	}

	// signature check
	isValid, err := youtubeCheckSignature(r, body)
	if err != nil {
		logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Error(err)
		return
	}
	if !isValid {
		logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Warnf("Got invalid signature")
		return
	}

	if r.Method == http.MethodPost {
		// looks to be some stream data
		data := &YTFeed{}

		// accept
		w.WriteHeader(http.StatusAccepted)

		err = xml.Unmarshal(body, data)
		if err != nil {
			logrus.Error(err)
			return
		}

		// does redis think we have this in the list?
		// I know the prior commit says HLL, I misunderstood what HLL is, and a normal redis set is fine
		found, err := f.settings.GetRedisCache().SIsMember(context.Background(), "ytnotifs", data.Entry.VideoId).Result()
		if err != nil {
			logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Error(err)
			return
		}
		// if it exists, we've already pinged about this -- this is probably a description or title change
		// because youtube doesn't differentiate uploads, edits and deletions :eyeroll:
		if found {
			return
		}

		f.pNotificationsSeen.WithLabelValues(string(_FirehoseServiceYoutube)).Inc()

		toNotify, err := getServersToNotify(f.db, data.Entry.ChannelId, _FirehoseServiceYoutube)
		if err != nil {
			logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Error(err)
			return
		}

		err = f.doNotifyServers(toNotify, &fhGoLive{
			Username: data.Entry.Author.Name,
			Service:  _FirehoseServiceYoutube,
			Category: "",
			Title:    data.Entry.Title,
			Link:     data.Entry.Link.Href,
		})
		if err != nil {
			logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Error(err)
			return
		}

		// now throw it in the redis
		err = f.settings.GetRedisCache().SAdd(context.Background(), "ytnotifs", data.Entry.VideoId).Err()
		if err != nil {
			logrus.WithField("firehose.platform", _FirehoseServiceYoutube).Error(err)
			return
		}
	}
}
