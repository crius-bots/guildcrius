package firehose

import (
	"github.com/sirupsen/logrus"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"time"
)

func (f *firehose) startConnections() error {
	// start by opening up a connection to glimesh
	err := f.glimwsc.Open()
	if err != nil {
		return err
	}

	notifs := []*struct {
		ExtID   string `db:"ext_id"`
		Service firehoseService
	}{}

	err = f.db.Select(&notifs, "SELECT ext_id, service FROM firehose__notifs GROUP BY ext_id, service;")
	if err != nil {
		return err
	}

	// start the youtube ticker for 23h55m
	f.ytTicker = time.NewTicker((time.Hour * 23) + (time.Minute * 55))
	go f.ytRenewer()

	for k, notif := range notifs {
		// first, is the service valid?
		if !notif.Service.IsValid() {
			// no?!
			// god knows how this has happened because we .IsValid before insert and postgres protects us.
			logrus.Errorf("Invalid service %s at index %d (ExtID: %s)", notif.Service, k, notif.ExtID)
			continue
		}

		f.pWatchCount.WithLabelValues(cc.PlatformDiscord.ToString(), string(notif.Service)).Inc()

		switch notif.Service {
		case _FirehoseServiceGlimesh:
			err = f.glimeshBeginSub(notif.ExtID)
		case _FirehoseServiceTwitch:
			// we need not do anything here because unlike youtube, Twitch supports infinite hooks
			break
		case _FirehoseServiceYoutube:
			err = f.ytBeginSub(notif.ExtID)
		}
	}

	return nil
}

func (f *firehose) ytRenewer() {
	for {
		select {
		case <-f.ytDone:
			return
		case <-f.ytTicker.C:
			// renew.
			// I don't think this causes duplicates, i presume it just bumps the time back up
			notifs := []string{}

			err := f.db.Select(&notifs, "SELECT ext_id FROM firehose__notifs WHERE service=$1", _FirehoseServiceYoutube)
			if err != nil {
				logrus.Error(err)
			}

			for _, notif := range notifs {
				err := f.ytBeginSub(notif)
				if err != nil {
					logrus.Error(err)
				}
			}
		}
	}
}
