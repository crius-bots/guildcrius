package mailer

import (
	"context"
	"github.com/bwmarrin/discordgo"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius/guildcrius/permissions"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Mailer",
	Creator:            "Ben <Ponkey364>",
	License:            "MPL-2.0",
	URL:                "https://gitlab.com/RPGPN/guildcrius/-/tree/hosted/plugins/mailer",
	Description:        "Mailbox system",
	SupportedPlatforms: cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "New Mailbox",
			HandlerName:        "newmailbox",
			Activator:          "newmailbox",
			Help:               "Make a new mailbox. Usage: `newmailbox [name] [channel]`",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Delete Mailbox",
			HandlerName:        "delmailbox",
			Activator:          "delmailbox",
			Help:               "Delete a mailbox. Usage: `delmailbox [name]`",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Move Mailbox",
			HandlerName:        "movemailbox",
			Activator:          "movemailbox",
			Help:               "Move a mailbox. Usage: `movemailbox [mailbox name] [to channel]`",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Send Mail",
			HandlerName:        "sendmail",
			Activator:          "sendmail",
			Help:               "Send some mail. Usage: `sendmail [mailbox name] [message] [optional: subject]`",
			SupportedPlatforms: cc.PlatformDiscord,
		},
	},
}

type mailer struct {
	perms    GuildCrius.Permissions
	settings CriusUtils.Settings
	session  *discordgo.Session
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	perms := GuildCrius.GetPermissions(ctx)
	settings := CriusUtils.GetSettings(ctx)
	session := ctx.Value(cc.PlatformDiscord.ToString()).(*discordgo.Session)

	m := &mailer{
		perms:    perms,
		settings: settings,
		session:  session,
	}

	return map[string]cc.CommandHandler{
		"newmailbox":  m.NewMailbox,
		"delmailbox":  m.DeleteMailbox,
		"movemailbox": m.MoveMailbox,
		"sendmail":    m.SendMail,
	}, nil
}

func (m *mailer) NewMailbox(mc *cc.MessageContext, args []string) error {
	if len(args) != 2 {
		mc.Send("Usage: `newmailbox [name] [channel]`")
		return nil
	}

	isAdmin, err := m.perms.IsServerAdmin(mc.Author.ID, mc.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		mc.Send(permissions.ErrNotSAdmin)
		return nil
	}

	sendTo := args[1]
	channel := sendTo[2 : len(sendTo)-1]

	_, err = m.settings.GetDB().Exec(
		"INSERT INTO mailer__mailbox (server_id, channel_id, mailbox_name) VALUES ($1,$2,$3)",
		mc.RealmID, channel, args[0])
	if err != nil {
		return err
	}

	return nil
}

func (m *mailer) DeleteMailbox(mc *cc.MessageContext, args []string) error {
	if len(args) != 1 {
		mc.Send("Usage: `delmailbox [name]`")
		return nil
	}

	isAdmin, err := m.perms.IsServerAdmin(mc.Author.ID, mc.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		mc.Send(permissions.ErrNotSAdmin)
		return nil
	}

	_, err = m.settings.GetDB().Exec(
		"DELETE FROM mailer__mailbox WHERE server_id=$1 AND mailbox_name=$2",
		mc.RealmID, args[0])
	if err != nil {
		return err
	}

	return nil
}

func (m *mailer) MoveMailbox(mc *cc.MessageContext, args []string) error {
	if len(args) < 2 || len(args) > 2 {
		mc.Send("Usage: `movemailbox [mailbox name] [to channel]`")
		return nil
	}

	isAdmin, err := m.perms.IsServerAdmin(mc.Author.ID, mc.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		mc.Send(permissions.ErrNotSAdmin)
		return nil
	}

	sendTo := args[1]
	channel := sendTo[2 : len(sendTo)-1]

	_, err = m.settings.GetDB().Exec(
		"UPDATE mailer__mailbox SET channel_id=$1 WHERE server_id=$2 AND mailbox_name=$3",
		channel, mc.RealmID, args[0])
	if err != nil {
		return err
	}

	return nil
}

func (m *mailer) SendMail(mc *cc.MessageContext, args []string) error {
	if len(args) < 2 || len(args) > 3 {
		mc.Send("Usage: sendmail [mailbox name] [message] [optional: subject]")
		return nil
	}

	mailbox := args[0]
	message := args[1]
	// subject may or may not exist
	var subject = "[no subject]"
	if len(args) == 3 {
		subject = args[2]
	}

	// look up which channel to send the mail to
	var channel string
	err := m.settings.GetDB().Get(&channel,
		"SELECT channel_id FROM mailer__mailbox WHERE mailbox_name=$1 AND server_id=$2",
		mailbox, mc.RealmID)
	if err != nil {
		return err
	}

	// make an embed
	embed := &discordgo.MessageEmbed{
		Title:  mailbox + " has mail!",
		Color:  0,
		Footer: &discordgo.MessageEmbedFooter{Text: "GuildCrius Mailer"},
		Author: &discordgo.MessageEmbedAuthor{Name: mc.Author.Username},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  subject,
				Value: message,
			},
		},
	}

	_, err = mc.SendTo(channel, &discordgo.MessageSend{Embed: embed})
	if err != nil {
		return err
	}

	return nil
}
