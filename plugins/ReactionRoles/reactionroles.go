package ReactionRoles

import (
	"context"
	"github.com/bwmarrin/discordgo"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius/guildcrius/permissions"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/RPGPN/criuscommander/v2/discord"
	"strconv"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "ReactionRoles",
	Creator:            "Ben <Ponkey364>",
	License:            "MPL-2.0",
	URL:                "https://gitlab.com/RPGPN/guildcrius/-/tree/hosted/plugins/ReactionRoles",
	Description:        "Commands for configuring reaction roles",
	SupportedPlatforms: cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "New ReactionRole Message",
			Help:               "Add a new reaction role message. Usage: `newrr [message content] [send to channel] ([emoji name] [role id])...`",
			HandlerName:        "newrr",
			Activator:          "newrr",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Append ReactionRole to Message",
			Help:               "Append a reaction role to message (existing RR message or not). Usage: `appendrr [message URL] ([emoji name] [role id])...`",
			HandlerName:        "appendrr",
			Activator:          "appendrr",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Remove Role from Message",
			Help:               "Removes a role from the message. Usage: `rmrole [message URL] [emoji name]...`",
			HandlerName:        "rmrole",
			Activator:          "rmrole",
			SupportedPlatforms: cc.PlatformDiscord,
		},
		{
			Name:               "Stop ReactionRoles on Message",
			Help:               "Stop giving out roles on reactions on the provided message. Usage: `stoprr [message URL] [delete msg (true/false)]`",
			HandlerName:        "stoprr",
			Activator:          "stoprr",
			SupportedPlatforms: cc.PlatformDiscord,
		},
	},
}

type DBReactRole struct {
	ReactionRoleID string `db:"rr_id"`
	ServerID       int    `db:"server_id"`
	MessageID      int    `db:"message_id"`
	GiveRole       string `db:"give_role"`
	Emoji          string
}

type reactionRoles struct {
	db      *sqlx.DB
	perms   GuildCrius.Permissions
	session *discordgo.Session
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	perms := GuildCrius.GetPermissions(ctx)
	settings := CriusUtils.GetSettings(ctx)
	bus := CriusUtils.GetBus(ctx)
	session := ctx.Value(cc.PlatformDiscord.ToString()).(*discordgo.Session)

	r := &reactionRoles{
		db:      settings.GetDB(),
		perms:   perms,
		session: session,
	}

	// register events
	bus.Subscribe(discord.EventMessageReactionAdd, r.onReactionAdd)
	bus.Subscribe(discord.EventMessageReactionRemove, r.onReactionRemove)

	return map[string]cc.CommandHandler{
		"newrr":    r.NewReactionRole,
		"appendrr": r.AppendRRToMessage,
		"stoprr":   r.StopRROnMessage,
		"rmrole":   r.RemRoleFromMessage,
	}, nil
}

func (r *reactionRoles) NewReactionRole(m *cc.MessageContext, args []string) error {
	if len(args) < 4 {
		m.Send("Usage: newrr [message content] [send to channel] ([emoji name] [role id])...")
		return nil
	}

	isAdmin, err := r.perms.IsServerAdmin(m.Author.ID, m.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		m.Send(permissions.ErrNotSAdmin)
		return nil
	}

	content := args[0]
	sendTo := args[1]
	channel := sendTo[2 : len(sendTo)-1]
	roles := args[2:]

	msgI, err := m.SendTo(channel, content)
	if err != nil {
		return err
	}

	msg := msgI.(*discordgo.Message)

	for i := 0; i <= len(roles)/2; i += 2 {
		giveRole := roles[i+1]

		emoji := getEmoji(roles[i])

		_, err = r.db.Exec("INSERT INTO reaction_roles (server_id, message_id, give_role, emoji) VALUES ($1,$2,$3,$4)",
			m.RealmID, msg.ID, giveRole, emoji.Name)
		if err != nil {
			return err
		}

		err = r.session.MessageReactionAdd(channel, msg.ID, emoji.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *reactionRoles) AppendRRToMessage(m *cc.MessageContext, args []string) error {
	if len(args) < 3 {
		m.Send("Usage: appendrr [message URL] ([emoji name] [role id])...")
		return nil
	}

	isAdmin, err := r.perms.IsServerAdmin(m.Author.ID, m.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		m.Send(permissions.ErrNotSAdmin)
		return nil
	}

	mServerID, channel, msgID := getMessageInfoFromURL(args[0])
	if mServerID != m.RealmID {
		m.Send("The target message must be in this realm")
		return nil
	}

	roles := args[1:]

	for i := 0; i <= len(roles)/2; i += 2 {
		giveRole := roles[i+1]

		emoji := getEmoji(roles[i])

		_, err = r.db.Exec("INSERT INTO reaction_roles (server_id, message_id, give_role, emoji) VALUES ($1,$2,$3,$4)",
			m.RealmID, msgID, giveRole, emoji.Name)
		if err != nil {
			return err
		}

		err = r.session.MessageReactionAdd(channel, msgID, emoji.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *reactionRoles) StopRROnMessage(m *cc.MessageContext, args []string) error {
	if len(args) < 2 {
		m.Send("Usage: `stoprr [message URL] [delete msg (true/false)]`")
		return nil
	}

	isAdmin, err := r.perms.IsServerAdmin(m.Author.ID, m.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		m.Send(permissions.ErrNotSAdmin)
		return nil
	}

	shouldDeleteMessage, err := strconv.ParseBool(args[1])
	if err != nil {
		return err
	}

	mServerID, channel, messageID := getMessageInfoFromURL(args[0])
	if mServerID != m.RealmID {
		m.Send("The target message must be in this realm")
		return nil
	}

	_, err = r.db.Exec("DELETE FROM reaction_roles WHERE server_id=$1 AND message_id=$2", mServerID, messageID)
	if err != nil {
		return err
	}

	if shouldDeleteMessage {
		err = r.session.ChannelMessageDelete(channel, messageID)
		return err
	}

	// otherwise, just clear the reactions
	err = r.session.MessageReactionsRemoveAll(channel, messageID)
	return err
}

func (r *reactionRoles) RemRoleFromMessage(m *cc.MessageContext, args []string) error {
	if len(args) < 2 {
		m.Send("Usage: `rmrole [message URL] [emoji name]...`")
		return nil
	}

	isAdmin, err := r.perms.IsServerAdmin(m.Author.ID, m.RealmID)
	if err != nil {
		return err
	}
	if !isAdmin {
		m.Send(permissions.ErrNotSAdmin)
		return nil
	}

	mServerID, channel, msgID := getMessageInfoFromURL(args[0])
	if mServerID != m.RealmID {
		m.Send("The target message must be in this realm")
		return nil
	}

	roles := args[1:]
	for _, emoji := range roles {
		emojiInfo := getEmoji(emoji)

		// remove from db before we remove from discord
		_, err = r.db.Exec("DELETE FROM reaction_roles WHERE server_id=$1 AND message_id=$2 AND emoji=$3", mServerID,
			msgID, emoji)
		if err != nil {
			return err
		}

		for {
			// we're removing so we (hopefully) won't need to bother with the before and after.
			reactions, err := r.session.MessageReactions(channel, msgID, emojiInfo.ID, 99, "", "")
			if err != nil {
				return err
			}

			if len(reactions) == 0 {
				break
			}

			for _, user := range reactions {
				err = r.session.MessageReactionRemove(channel, msgID, emojiInfo.ID, user.ID)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (r *reactionRoles) onReactionAdd(reaction *discordgo.MessageReactionAdd) {
	shouldContinue, err := checkReaction(r.session, reaction.MessageReaction)
	if err != nil {
		logrus.Error(err)
		return
	}

	if !shouldContinue {
		return
	}

	role, err := getReactRoleFromDiscord(r.db, reaction.MessageReaction)
	if err != nil {
		logrus.Error(err)
		return
	}

	// ok, we need to process.
	err = r.session.GuildMemberRoleAdd(reaction.GuildID, reaction.UserID, role.GiveRole)
	if err != nil {
		logrus.Error(err)
		return
	}
}

func (r *reactionRoles) onReactionRemove(reaction *discordgo.MessageReactionRemove) {
	shouldContinue, err := checkReaction(r.session, reaction.MessageReaction)
	if err != nil {
		logrus.Error(err)
		return
	}

	if !shouldContinue {
		return
	}

	role, err := getReactRoleFromDiscord(r.db, reaction.MessageReaction)
	if err != nil {
		logrus.Error(err)
		return
	}

	// ok, we need to process.
	err = r.session.GuildMemberRoleRemove(reaction.GuildID, reaction.UserID, role.GiveRole)
	if err != nil {
		logrus.Error(err)
		return
	}
}
