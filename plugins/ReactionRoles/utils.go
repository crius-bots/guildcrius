package ReactionRoles

import (
	"database/sql"
	"github.com/bwmarrin/discordgo"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"unicode/utf8"
)

func getReactRoleFromDiscord(db *sqlx.DB, reaction *discordgo.MessageReaction) (*DBReactRole, error) {
	dbRRItem := &DBReactRole{}

	err := db.Get(dbRRItem, "SELECT * FROM reaction_roles WHERE server_id=$1 AND message_id=$2 AND emoji=$3",
		reaction.GuildID, reaction.MessageID, reaction.Emoji.Name)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return dbRRItem, nil
}

func checkReaction(session *discordgo.Session, reaction *discordgo.MessageReaction) (bool, error) {
	if reaction.UserID == session.State.User.ID {
		return false, nil
	}

	msg, err := session.ChannelMessage(reaction.ChannelID, reaction.MessageID)
	if err != nil {
		return false, err
	}

	// ignore any messages that aren't ours, they won't be in the database
	if msg.Author.ID != session.State.User.ID {
		return false, nil
	}

	return true, nil
}

// to explain this mess:
// when we get an emoji that's *actually* an emoji (i.e. has a unicode code point) we need to *know* it's just a
// codepoint and so there will be no <_name_:_id_> format. it's horrid, but works lol
func getEmoji(emoji string) *discordgo.Emoji {
	if utf8.RuneCountInString(emoji) == 1 {
		return &discordgo.Emoji{
			ID:   emoji,
			Name: emoji,
		}
	} else {
		split := strings.Split(emoji, ":")
		emojiName := split[len(split)-2]

		return &discordgo.Emoji{
			ID:   emoji[:len(emoji)-1],
			Name: emojiName,
		}
	}
}

var msgURLRegex = regexp.MustCompile("(?:https?://discord(?:app)?.com)(?:/channels/)(\\d{18})/(\\d{18})/(\\d{18})")

// returns: server ID, channel ID, message ID
func getMessageInfoFromURL(url string) (string, string, string) {
	matches := msgURLRegex.FindStringSubmatch(url)

	return matches[1], matches[2], matches[3]
}
