package permissions

import (
	"context"
	"database/sql"
	"gitlab.com/RPGPN/crius/guildcrius/settings"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

const (
	ErrNotOwner  = "You must be the bot owner to run this command"
	ErrNotSAdmin = "You must be a server administrator to run this command"
	ErrNotSOwner = "You must be a server owner to run this command"
	ErrNotSMod   = "You must be a server moderator to run this command"
)

var Info = &cc.PluginJSON{
	Name:               "Permissions",
	Creator:            "Ben <ponkey364>",
	License:            "BSD-3-Clause",
	URL:                "https://gitlab.com/RPGPN/guildcrius/-/tree/main/permissions",
	Description:        "Commands for configuring permissions",
	SupportedPlatforms: cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "Give Perm",
			Activator:          "giveperm",
			Help:               "Give Someone Permissions",
			HandlerName:        "giveperm",
			SupportedPlatforms: cc.PlatformDiscord,
			Subcommands: []*cc.PJCommand{
				{

					Name:               "Add Server Admin",
					Activator:          "serverAdmin",
					Help:               "Add a new server admin. Usage: giveperm serverAdmin [user]" + CMD_SERVER_OWNER_ONLY,
					SupportedPlatforms: cc.PlatformDiscord,
					HandlerName:        "serverAdmin",
				},
				{
					Name:               "Add Server Moderator",
					Activator:          "serverMod",
					Help:               "Add a new server moderator. Usage: giveperm serverModerator [user]" + CMD_SERVER_ADMINS_ONLY,
					SupportedPlatforms: cc.PlatformDiscord,
					HandlerName:        "serverMod",
				},
			},
		},
		{
			Name:               "Take Perm",
			Activator:          "takeperm",
			Help:               "Remove Someone's Permissions",
			HandlerName:        "takeperm",
			SupportedPlatforms: cc.PlatformDiscord,
			Subcommands: []*cc.PJCommand{
				{
					Name:               "Remove Server Admin",
					Activator:          "serverAdmin",
					Help:               "Removes a server admin. Usage: takeperm serverAdmin [user]" + CMD_SERVER_OWNER_ONLY,
					SupportedPlatforms: cc.PlatformDiscord,
					HandlerName:        "serverAdmin",
				},
				{
					Name:               "Remove Server Moderator",
					Activator:          "serverMod",
					Help:               "Removes a server moderator. Usage: takeperm serverMod [user]" + CMD_SERVER_ADMINS_ONLY,
					SupportedPlatforms: cc.PlatformDiscord,
					HandlerName:        "serverMod",
				},
			},
		},
	},
}

// what's going on:
// we need to have the commands registered here **BUT ALSO** a ref to the struct on ctx, so we need to faff.
func Setup() *Permissions {
	return &Permissions{}
}

// Technically registered like a plugin, but is going to be required to the point that it is included by default
func (p *Permissions) Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	settingsPlugin := ctx.Value("settings").(*settings.Settings)

	p.globalSettings = settingsPlugin
	p.ctx = ctx

	return map[string]cc.CommandHandler{
		"giveperm/serverAdmin": p.AddServerAdmin,
		"takeperm/serverAdmin": p.RemoveServerAdmin,
		"giveperm/serverMod":   p.AddServerModerator,
		"takeperm/serverMod":   p.RemoveServerModerator,
	}, nil
}

type ServerPerms struct {
	PermID     int    `db:"perm_id"`
	ServerID   string `db:"server_id"`
	Permission int
	UserID     string `db:"user_id"`
}

type Permissions struct {
	globalSettings *settings.Settings
	ctx            context.Context
}

func (p *Permissions) getServerPerms(userID string, serverID string) (*ServerPerms, error) {
	perms := &ServerPerms{}
	err := p.globalSettings.GetDB().Get(perms, "SELECT * FROM server_permissions WHERE server_id=$1 AND user_id=$2",
		serverID, userID)

	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return perms, nil
}

func (p *Permissions) removeRole(userID string, serverID string) error {
	_, err := p.globalSettings.GetDB().Exec("DELETE FROM server_permissions WHERE user_id=$1 AND server_id=$2",
		userID, serverID)

	return err
}
