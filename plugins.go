package main

import (
	"gitlab.com/RPGPN/crius-plugin-botinfo"
	"gitlab.com/RPGPN/crius-plugin-info"
	"gitlab.com/RPGPN/crius-plugin-links"
	"gitlab.com/RPGPN/crius-plugin-polls"
	"gitlab.com/RPGPN/crius-plugin-quotes"
	"gitlab.com/RPGPN/crius/guildcrius/plugins/ReactionRoles"
	"gitlab.com/RPGPN/crius/guildcrius/plugins/firehose"
	"gitlab.com/RPGPN/crius/guildcrius/plugins/mailer"
	"gitlab.com/RPGPN/crius/guildcrius/plugins/presence"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

func loadPlugins(cmder *cc.Commander) error {
	if err := cmder.SetupPluginFromConfig(PromSetup, PromPluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_botinfo.Setup, crius_plugin_botinfo.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(Links.Setup, Links.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_quotes.Setup, crius_plugin_quotes.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_info.Setup, crius_plugin_info.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_polls.Setup, crius_plugin_polls.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(presence.Setup, presence.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(ReactionRoles.Setup, ReactionRoles.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(mailer.Setup, mailer.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(firehose.Setup, firehose.PluginInfo); err != nil {
		return err
	}

	return nil
}
